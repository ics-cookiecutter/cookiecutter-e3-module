# e3 conda module cookiecutter template

[Cookiecutter](https://github.com/audreyr/cookiecutter) template for e3 conda modules.

## Quickstart

Install the latest Cookiecutter if you haven't installed it yet:

```
$ pip install cookiecutter
```

Generate an EPICS/E3 module:

```
$ cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-module.git
```

As this is not easy to remember, you can add an alias in your `~/.bash_profile`:

```
alias e3-module='cookiecutter git+https://gitlab.esss.lu.se/ics-cookiecutter/cookiecutter-e3-module.git'
```

