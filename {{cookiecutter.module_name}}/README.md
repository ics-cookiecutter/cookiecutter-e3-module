
{{ cookiecutter.module_name }}
======
{{ cookiecutter.company }} Site-specific EPICS module : {{ cookiecutter.module_name }}

Additonal information:

* [Documentation]({{ cookiecutter.documentation_page }})
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/{{ cookiecutter.module_name }}-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)